package com.example.service;

import com.example.entity.Measurement;
import com.google.gson.Gson;

import java.io.IOException;

public class MeasurementRESTService {
    private static String url = "http://localhost:8080/Wetterstation/api.php?r=measurement";
    private RESTService restService;

    public MeasurementRESTService() {
        this.restService = new RESTService();
    }

    public Measurement[] get() throws IOException {
        String response = this.restService.sendGETRequest(url);

        Gson gson = new Gson();

        return gson.fromJson(response, Measurement[].class);
    }

    public Measurement get(long id) throws IOException {
        String response = this.restService.sendGETRequest(url + "/" + id);

        Gson gson = new Gson();

        return gson.fromJson(response, Measurement.class);
    }

    public String post(Measurement measurement) throws IOException {
        return restService.sendPOSTRequest(url, measurement);
    }

    public String delete(long id) throws IOException {
        return restService.sendDELETERequest(url + "/" + id);
    }

    public String put(Measurement measurement) throws IOException {
        return restService.sendPUTRequest(url + "/" + measurement.getId(), measurement);
    }
}
