package com.example.service;

import com.example.entity.Measurement;

public class MeasurementHibernateService extends HibernateService<Measurement> {

    public MeasurementHibernateService() {
        super();
        this.typeParameterClass = Measurement.class;
    }
}
