package com.example.service;

import com.example.Main;
import com.example.entity.IEntity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.util.List;

public abstract class HibernateService<T extends IEntity> {

    protected Class<T> typeParameterClass;

    protected HibernateService() {
    }

    public void saveOrUpdate(T entity) {
        EntityManagerFactory factory = Main.ENTITY_MANAGER_FACTORY;
        EntityManager entitymanager = factory.createEntityManager();
        entitymanager.getTransaction().begin();

        if (entity.getId() != 0) {
            // Update
            entitymanager.merge(entity);
        } else {
            // Create
            entitymanager.persist(entity);
        }
        entitymanager.getTransaction().commit();

        entitymanager.close();
    }

    public List<T> getAll() {
        EntityManagerFactory factory = Main.ENTITY_MANAGER_FACTORY;
        EntityManager entityManager = factory.createEntityManager();

        //Between
        Query query = entityManager.createQuery("Select e " + "from " + this.typeParameterClass.getSimpleName() + " e ");

        return query.getResultList();
    }

    public T get(long id) {
        EntityManagerFactory emfactory = Main.ENTITY_MANAGER_FACTORY;
        EntityManager entitymanager = emfactory.createEntityManager();

        return entitymanager.find(this.typeParameterClass, id);
    }

    public T get(T entity) {
        return get(entity.getId());
    }

    public void delete(long id) {
        EntityManagerFactory emfactory = Main.ENTITY_MANAGER_FACTORY;
        EntityManager entitymanager = emfactory.createEntityManager();
        entitymanager.getTransaction().begin();

        T entity = entitymanager.find(this.typeParameterClass, id);
        entitymanager.remove(entity);
        entitymanager.getTransaction().commit();
        entitymanager.close();
    }

    public void delete(T entity) {
        delete(entity.getId());
    }
}
