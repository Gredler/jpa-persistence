package com.example.service;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class RESTService implements IRESTService {

    public RESTService() {
    }

    private HttpURLConnection getConnection(String address, String method) throws IOException {
        URL url = new URL(address);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod(method);

        return connection;
    }

    public String sendGETRequest(String address) throws IOException {
        HttpURLConnection connection = getConnection(address, "GET");
        connection.setRequestMethod("GET");

        checkResponseCode(connection, 200);

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

        String response = bufferedReader.readLine();
        if (response == null) {
            response = "ERROR";
        }
        connection.disconnect();
        return response;
    }

    @Override
    public String sendPOSTRequest(String address, Object object) throws IOException {
        HttpURLConnection connection = getConnection(address, "POST");

        sendBody(connection, object);

        checkResponseCode(connection, HttpURLConnection.HTTP_CREATED);

        connection.getInputStream();
        String response = (connection.getResponseMessage());
        connection.disconnect();
        return response;
    }

    @Override
    public String sendDELETERequest(String address) throws IOException {
        HttpURLConnection connection = getConnection(address, "DELETE");

        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.connect();

        checkResponseCode(connection, 200);

        String response = (connection.getResponseMessage());
        connection.disconnect();
        return response;
    }

    @Override
    public String sendPUTRequest(String address, Object object) throws IOException {
        HttpURLConnection connection = getConnection(address, "PUT");

        sendBody(connection, object);

        checkResponseCode(connection, 200);

        connection.getInputStream();
        String response = (connection.getResponseMessage());
        connection.disconnect();
        return response;
    }

    private void checkResponseCode(HttpURLConnection connection, int code) throws IOException {
        if (connection.getResponseCode() != code) {
            throw new RuntimeException("Failed: HTTP error code: " + connection.getResponseCode());
        }
    }

    private void sendBody(HttpURLConnection connection, Object object) throws IOException {
        OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
        Gson g = new Gson();
        String json = g.toJson(object);
        out.write(json);
        out.close();
    }
}
