package com.example.service;

import java.io.IOException;

public interface IRESTService {
    String sendGETRequest(String address) throws IOException;

    String sendPOSTRequest(String address, Object object) throws IOException;

    String sendDELETERequest(String address) throws IOException;

    String sendPUTRequest(String address, Object object) throws IOException;
}
