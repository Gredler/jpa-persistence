package com.example.controller;

import com.example.entity.Measurement;
import com.example.enums.Exports;
import com.example.service.MeasurementRESTService;
import com.example.thread.ExportThread;
import com.example.thread.JPAStartThread;
import com.example.thread.LoadingThread;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import tornadofx.control.DateTimePicker;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class FXMLController implements Initializable {

    private Thread loadingThread;

    @FXML
    private AnchorPane rootPane;

    @FXML
    private TextField txtHumi;

    @FXML
    private TextField txtTemp;

    @FXML
    private Button btnCreate;

    @FXML
    private Button btnExport;

    private DateTimePicker dateTimePicker;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        loadingThread = new LoadingThread();

        this.dateTimePicker = new DateTimePicker();
        dateTimePicker.setLayoutX(529);
        dateTimePicker.setLayoutY(67);
        rootPane.getChildren().add(dateTimePicker);

        // force the field to be numeric only
        txtTemp.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("^(-)?[0-9]*$")) {
                txtTemp.setText(oldValue);
            }
        });

        txtHumi.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                txtHumi.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
    }

    @FXML
    private void exportData() {
        Stage stage = (Stage) btnExport.getScene().getWindow();
        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON", "*.json"));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("XML", "*.xml"));

        //Show save file dialog
        File file = fileChooser.showSaveDialog(stage);

        Exports exportType = null;

        if (file != null) {
            if (file.getName().toLowerCase().endsWith(".json")) {
                exportType = Exports.JSON;
            } else if (file.getName().toLowerCase().endsWith(".xml")) {
                exportType = Exports.XML;
            }

            if (exportType != null) {
                new ExportThread(exportType, file);
            }
        }
    }

    @FXML
    private void createMeasurement() {
        MeasurementRESTService measurementRESTService = new MeasurementRESTService();
        Measurement measurement = new Measurement();

        // only set when a value has been entered - the default is 0 anyway
        if (!txtTemp.getText().equals("")) {
            measurement.setTemperature(Integer.parseInt(txtTemp.getText()));
        }

        if (!txtHumi.getText().equals("")) {
            measurement.setHumidity(Integer.parseInt(txtHumi.getText()));
        }

        // format the dateTime correctly
        String dateTime = dateTimePicker.getDateTimeValue().toString();
        if (dateTime.length() <= 16) {
            dateTime = dateTime.replace('T', ' ');
            measurement.setTimestamp(dateTime);

            System.out.println(measurement);
            dateTimePicker.setDateTimeValue(null);
            txtHumi.setText("");
            txtTemp.setText("");

            try {
                measurementRESTService.post(measurement);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
