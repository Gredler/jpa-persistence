package com.example.controller;

import com.example.thread.JPAStartThread;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class StartController implements Initializable {

    @FXML
    private ProgressBar progress;

    @FXML
    private AnchorPane rootPane;

    @FXML
    private Label lblText;

    @FXML
    private Label lblDone;

    @FXML
    private Button okButton;

    private Thread javaFXThread;

    private JPAStartThread startThread;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        javaFXThread = Thread.currentThread();

        startThread = new JPAStartThread(progress);

        okButton.setVisible(false);
        lblDone.setVisible(false);

        progress.progressProperty().addListener((observable, oldValue, newValue) -> {
            if ((double) newValue == 1) {
                lblText.setVisible(false);
                lblDone.setVisible(true);
                okButton.setVisible(true);
            }
        });
    }

    @FXML
    private void changeScene() throws IOException {
        Stage stage = (Stage) progress.getScene().getWindow();

        Parent root = FXMLLoader.load(getClass().getResource("../fxml/view.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

}
