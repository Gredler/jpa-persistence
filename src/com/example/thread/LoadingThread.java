package com.example.thread;

public class LoadingThread extends JPAThread {

    private DataThread dataThread;

    public LoadingThread() {
        super();

        dataThread = new DataThread();

        this.start();
    }

    @Override
    public void run() {
        while (true) {
            restMeasurements = dataThread.restMeasurements;
            measurements = dataThread.measurements;

            if (dataExists()) {
                if (restMeasurements.length < measurements.size()) {
                    deleteMeasurement();
                } else if (restMeasurements.length > measurements.size()) {
                    saveNewMeasurements();
                }
            }
            // TODO add a method for updated measurements + maybe a sleep ... maybe
        }
    }
}

class DataThread extends JPAThread {
    DataThread() {
        super();
        this.start();
    }

    @Override
    public void run() {
        while (true) {
            restMeasurements = loadRestMeasurements();
            measurements = measurementHibernateService.getAll();

            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}