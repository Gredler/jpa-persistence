package com.example.thread;

import com.example.entity.Measurement;
import com.example.service.MeasurementHibernateService;
import com.example.service.MeasurementRESTService;
import com.sun.javaws.progress.Progress;
import javafx.scene.control.ProgressBar;

import java.io.IOException;
import java.util.List;

public abstract class JPAThread extends Thread {
    protected MeasurementRESTService measurementRESTService;
    protected MeasurementHibernateService measurementHibernateService;

    protected Measurement[] restMeasurements;
    protected List<Measurement> measurements;

    protected ProgressBar progressBarToChange;

    protected JPAThread() {
        setDaemon(true);

        this.measurementHibernateService = new MeasurementHibernateService();
        this.measurementRESTService = new MeasurementRESTService();
    }

    protected JPAThread(ProgressBar progressBar) {
        this();
        this.progressBarToChange = progressBar;
    }

    protected Measurement[] loadRestMeasurements() {
        Measurement[] restMeasurements = null;

        try {
            restMeasurements = measurementRESTService.get();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return restMeasurements;
    }

    protected void saveNewMeasurements() {
        if (dataExists()) {
            for (int i = measurements.size(); i < restMeasurements.length; i++) {
                measurementHibernateService.saveOrUpdate(restMeasurements[i]);
                measurements.add(restMeasurements[i]);

                // change the progressBar if it exists
                if (progressBarToChange != null) {
                    progressBarToChange.setProgress((double)i / (restMeasurements.length - 1));
                }
            }

            System.out.println("DB mes: " + measurements.size());
        }
    }

    protected void deleteMeasurement() {
        if (dataExists()) {
            for (int i = 0; i < measurements.size(); i++) {
                Measurement m = measurements.get(i);
                Measurement restM = restMeasurements[i];

                if(m.getId() != restM.getId()) {
                    measurementHibernateService.delete(m);

                    // break out of the loop because everything after this is unreliable
                    break;
                }
            }
        }
    }

    protected boolean dataExists() {
        return (measurements != null && restMeasurements != null);
    }
}
