package com.example.thread;

import javafx.scene.control.ProgressBar;

public class JPAStartThread extends JPAThread {

    public JPAStartThread() {
        super();
        this.start();
    }

    public JPAStartThread(ProgressBar progressBar) {
        super(progressBar);
        this.start();
    }

    @Override
    public void run() {
        restMeasurements = loadRestMeasurements();
        measurements = measurementHibernateService.getAll();

        saveNewMeasurements();
    }
}
