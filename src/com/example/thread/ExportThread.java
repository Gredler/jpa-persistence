package com.example.thread;

import com.example.entity.Measurement;
import com.example.entity.MeasurementList;
import com.example.enums.Exports;
import com.example.service.MeasurementHibernateService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.util.List;

public class ExportThread extends Thread {

    private Exports exportType;
    private MeasurementHibernateService measurementHibernateService;
    private File file;

    public ExportThread(Exports exportType, File file) {
        this.exportType = exportType;
        this.file = file;

        this.measurementHibernateService = new MeasurementHibernateService();

        this.start();
    }

    @Override
    public void run() {
        switch (this.exportType) {
            case XML:
                exportXml();
                break;
            case JSON:
                exportJson();
                break;
            default:
                break;
        }
    }

    private void exportXml() {
        String xmlString = "";

        List<Measurement> measurements = measurementHibernateService.getAll();

        MeasurementList measurementList = new MeasurementList(measurements);

        try {
            JAXBContext context = JAXBContext.newInstance(MeasurementList.class);
            Marshaller m = context.createMarshaller();

            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); // To format XML

            StringWriter sw = new StringWriter();
            m.marshal(measurementList, sw);
            xmlString = sw.toString();
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        saveFile(xmlString);
    }

    private void exportJson() {
        // get the type of the list
        Type listType = new TypeToken<List<Measurement>>() {
        }.getType();
        List<Measurement> measurementList = measurementHibernateService.getAll();

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(measurementList, listType);

        saveFile(json);
    }

    private void saveFile(String content) {
        try {
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(content);
            fileWriter.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
