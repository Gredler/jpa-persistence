package com.example;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main extends Application {

    public static EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("Hibernate");

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("fxml/start.fxml"));
        primaryStage.setResizable(false);
        primaryStage.setTitle("JPA");
        primaryStage.setScene(new Scene(root));

        // close the entity manager when the window closes
        primaryStage.setOnCloseRequest(event -> ENTITY_MANAGER_FACTORY.close());

        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }


}
