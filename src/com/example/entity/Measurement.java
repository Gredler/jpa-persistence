package com.example.entity;

import javax.persistence.*;

@Entity
@Table
@Cacheable
public class Measurement implements IEntity<Measurement> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String timestamp;
    private double temperature;
    private double humidity;

    public Measurement() {
    }

    public Measurement(long id, String timestamp, double temperature, double humidity) {
        this.id = id;
        this.timestamp = timestamp;
        this.temperature = temperature;
        this.humidity = humidity;
    }

    public long getId() {
        return id;
    }

    @Override
    public void update(Measurement object) {
        this.humidity = object.humidity;
        this.temperature = object.temperature;
        this.timestamp = object.timestamp;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    @Override
    public String toString() {
        return "Measurement{" +
                "id=" + id +
                ", timestamp='" + timestamp + '\'' +
                ", temperature=" + temperature +
                ", humidity=" + humidity +
                '}';
    }
}
