package com.example.entity;

public interface IEntity<T extends IEntity> {
    long getId();
    void update(T object);
}
