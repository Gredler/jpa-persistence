package com.example.entity;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Needed because i haven't found a proper method to marshal a List of measurements just yet
 * TODO: ^ figure that out
 * This is kind of a hack to be honest
 */
@XmlRootElement
public class MeasurementList {
    private List<Measurement> measurement;

    public MeasurementList() {
    }

    public MeasurementList(List<Measurement> measurements) {
        this.measurement = measurements;
    }

    // Apparently getter and setter are mandatory for XML exporting
    public List<Measurement> getMeasurement() {
        return measurement;
    }

    public void setMeasurement(List<Measurement> measurement) {
        this.measurement = measurement;
    }
}
